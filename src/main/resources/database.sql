-- Create Language table
CREATE TABLE IF NOT EXISTS `language` (
  `id` int NOT NULL AUTO_INCREMENT,
  `locale` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Create User table
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(17) NOT NULL,
  `language_id` int NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`language_id`) REFERENCES `language` (`id`)
);