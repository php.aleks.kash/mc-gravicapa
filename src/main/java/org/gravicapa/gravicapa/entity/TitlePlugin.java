package org.gravicapa.gravicapa.entity;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.command.CommandSender;
import org.gravicapa.gravicapa.Gravicapa;
import org.gravicapa.gravicapa.entity.LeapingSpider;

import java.util.Arrays;

public class TitlePlugin implements Listener {

    static Gravicapa plugin;
    static int titleFadeIn = 10; // Время появления сообщения title (в тиках)
    static int titleStay = 70; // Время задержки сообщения title (в тиках)
    static int titleFadeOut = 20; // Время исчезания сообщения title (в тиках)
    static String titleText = "Привет, мир!"; // Текст сообщения title
    static float titleSize = 23.0f; // Размер текста сообщения title
    static int titleX = -100; // Горизонтальное положение текста сообщения title (от -100 до 100)
    static int titleY = 0; // Вертикальное положение текста сообщения title (от 0 до 100)

//    @Override
//    public void onEnable() {
//        // Запускаем задачу, которая выводит сообщения title каждую секунду
//        new BukkitRunnable() {
//            @Override
//            public void run() {
//                for (Player player : Bukkit.getOnlinePlayers()) {
//                    sendTitle(player);
//                }
//            }
//        }.runTaskTimer(this, 0L, 20L);
//    }

    public TitlePlugin(Gravicapa plugin) {
        LeapingSpider.plugin = plugin;
    }

    // Отправка сообщения title указанному игроку
    public static void sendTitle(Player player, CommandSender sender, String[] args) {

        if (args.length < 4) {
            sender.sendMessage(ChatColor.RED + "Использование: /titleconfig <размер> <X> <Y> <текст>");
            //return true;
        }

//        titleSize = TitlePlugin.titleSize;
//        titleX =
//        titleY =
//        titleText =

        try {
            if(args[0] != null)
            {
                titleSize = Float.parseFloat(args[0]);
            }

            if(args[1] != null)
            {
                titleX = Integer.parseInt(args[1]);
            }

            if(args[2] != null)
            {
                titleY = Integer.parseInt(args[2]);
            }

            if(args[3] != null)
            {
                titleText = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
            }

            sender.sendMessage(ChatColor.GREEN + "Параметры title успешно настроены!");
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Ошибка: неверный формат числа.");
        } catch (ArrayIndexOutOfBoundsException e) {
            sender.sendMessage(ChatColor.RED + "Ошибка: не все параметры указаны.");
        }

        player.sendTitle(ChatColor.translateAlternateColorCodes('&', titleText),
                null, titleFadeIn, titleStay, titleFadeOut);
    }

    public static void sendTitle2(Player player) {
        player.sendTitle(ChatColor.translateAlternateColorCodes('&', titleText),
                null, titleFadeIn, titleStay, titleFadeOut);
    }

//    // Команда для настройки параметров title
//    private class TitleConfigCommand implements CommandExecutor {
//        @Override
//        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
//            if (args.length < 4) {
//                sender.sendMessage(ChatColor.RED + "Использование: /titleconfig <размер> <X> <Y> <текст>");
//                return true;
//            }
//
//            try {
//                titleSize = Float.parseFloat(args[0]);
//                titleX = Integer.parseInt(args[1]);
//                titleY = Integer.parseInt(args[2]);
//                titleText = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
//                sender.sendMessage(ChatColor.GREEN + "Параметры title успешно настроены!");
//            } catch (NumberFormatException e) {
//                sender.sendMessage(ChatColor.RED + "Ошибка: неверный формат числа.");
//            } catch (ArrayIndexOutOfBoundsException e) {
//                sender.sendMessage(ChatColor.RED + "Ошибка: не все параметры указаны.");
//            }
//
//            return true;
//        }
//    }
}
