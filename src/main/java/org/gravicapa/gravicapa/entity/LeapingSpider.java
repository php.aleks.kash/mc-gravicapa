package org.gravicapa.gravicapa.entity;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attributable;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.gravicapa.gravicapa.Gravicapa;
import org.gravicapa.gravicapa.Message;

public class LeapingSpider implements Listener {

    public static Gravicapa plugin;
    public static final String TEXT_ID_LEAPING_SPIDER = "leaping_spider";

    public static final String DEFAULT_TRANSLATE_LEAPING_SPIDER = "Leaping Spider";
    public static final String RU_TRANSLATE_LEAPING_SPIDER = "Липкий паук";

    private static final String lang = "en"; // for default

    public LeapingSpider(Gravicapa plugin) {
        LeapingSpider.plugin = plugin;
    }

    public static String chooseLocale(String locale){ // for test instead of DEFAULT_TRANSLATE_LEAPING_SPIDER
        if(locale.equals("en")){
            return DEFAULT_TRANSLATE_LEAPING_SPIDER;
        } else if (locale.equals("ru")) {
            return RU_TRANSLATE_LEAPING_SPIDER;
        }
        return "";
    }

    public static void createLeapingSpider(Location location) {
        Message.className = "LeapingSpider";
        Spider spider = location.getWorld().spawn(location, Spider.class);
        spider.setCustomName(ChatColor.DARK_GRAY + Message.getTranslate(TEXT_ID_LEAPING_SPIDER
                , DEFAULT_TRANSLATE_LEAPING_SPIDER));

        spider.setCustomNameVisible(true);
        Attributable spiderAt = spider;
        AttributeInstance attribute = spiderAt.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        attribute.setBaseValue(100);
        spider.setHealth(100);

        new BukkitRunnable(){
            @Override
            public void run() {
                if(!spider.isDead()) {
                    if (spider.getTarget() == null) {
                        for (Entity entity : spider.getNearbyEntities(10, 10, 10)) {
                            if (entity instanceof Player) {
                                Player player = (Player) entity;
                                spider.setTarget(player);
                            }
                        }
                    }
                    else {
                        LivingEntity target = spider.getTarget();
                        if (target.getLocation().distanceSquared(spider.getLocation()) > 25) {
                            spider.getWorld().playSound(spider.getLocation(), Sound.ENTITY_WITHER_SHOOT, 5, 5);
                            spider.getWorld().spawnParticle(Particle.CAMPFIRE_COSY_SMOKE, spider.getLocation(), 10);
                            spider.setVelocity(target.getLocation().add(0, 2, 0).subtract(spider.getLocation()).toVector().multiply(0.275));
                        }
                    }
                }
                else {
                    cancel();
                }
            }
        }.runTaskTimer(LeapingSpider.plugin,0L, 20L);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Spider){
            if(event.getDamager().getCustomName() != null && event.getDamager().getCustomName().equals(ChatColor.DARK_GRAY + Message.getTranslate(TEXT_ID_LEAPING_SPIDER, chooseLocale(lang)))){
                if(event.getEntity() instanceof Player){
                    Player player = (Player) event.getEntity();
                    player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER,60,0));
                }
            }
        }
    }

}
