package org.gravicapa.gravicapa.entity;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.gravicapa.gravicapa.Message;

import java.util.ArrayList;
import java.util.List;

public class ItemManager {
    public static ItemStack ZombieKnightSpawnEgg;

    public static final String TEXT_ID_ZOMBIE_KNIGHT_SPAWN_EGG = "zombie_knight_spawn_egg";
    public static final String DEFAULT_TRANSLATE_ZOMBIE_KNIGHT_SPAWN_EGG = "Zombie Knight Spawn Egg";
    public static final String RU_TRANSLATE_ZOMBIE_KNIGHT_SPAWN_EGG = "Яйцо призыва зомби-рыцаря";

    public static final String TEXT_ID_SPAWNS_ZOMBIE_KNIGHT = "spawns_zombie_knight";
    public static final String DEFAULT_TRANSLATE_SPAWNS_ZOMBIE_KNIGHT = "spawns Zombie Knight";
    public static final String RU_TRANSLATE_SPAWNS_ZOMBIE_KNIGHT = "порождает зомби-рыцаря";
    private static final String lang = "en";

    public static String chooseLocale(String locale, int n){
        if(n==1){
            if(locale.equals("en")){
                return DEFAULT_TRANSLATE_ZOMBIE_KNIGHT_SPAWN_EGG;
            } else if (locale.equals("ru")) {
                return RU_TRANSLATE_ZOMBIE_KNIGHT_SPAWN_EGG;
            }
        } else if (n==2) {
            if(locale.equals("en")){
                return DEFAULT_TRANSLATE_SPAWNS_ZOMBIE_KNIGHT;
            } else if (locale.equals("ru")) {
                return RU_TRANSLATE_SPAWNS_ZOMBIE_KNIGHT;
            }
        }

        return "";
    }

    public static void createZombieKnightSpawnEgg() {
        Message.className = "ItemManager";
        ItemStack item = new ItemStack(Material.ZOGLIN_SPAWN_EGG,1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§6" + Message.getTranslate(TEXT_ID_ZOMBIE_KNIGHT_SPAWN_EGG, DEFAULT_TRANSLATE_ZOMBIE_KNIGHT_SPAWN_EGG));
        List<String> lore = new ArrayList<>();
        lore.add("§7" + Message.getTranslate(TEXT_ID_SPAWNS_ZOMBIE_KNIGHT, DEFAULT_TRANSLATE_SPAWNS_ZOMBIE_KNIGHT));
        meta.setLore(lore);
        item.setItemMeta(meta);
        ZombieKnightSpawnEgg = item;
    }

}
