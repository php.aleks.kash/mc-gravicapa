package org.gravicapa.gravicapa.entity;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.gravicapa.gravicapa.Message;

public class ZombieKnightSpawnEgg implements Listener {

    public static final String TEXT_ID_ZOMBIE_KNIGHT = "zombie_knight";
    public static final String DEFAULT_TRANSLATE_ZOMBIE_KNIGHT = "Zombie Knight";
    public static final String RU_TRANSLATE_ZOMBIE_KNIGHT = "Зомби-рыцарь";

    private static final String lang = "en";

    public ZombieKnightSpawnEgg() {}

    public static String chooseLocale(String locale){
        if(locale.equals("en")){
            return DEFAULT_TRANSLATE_ZOMBIE_KNIGHT;
        } else if (locale.equals("ru")) {
            return RU_TRANSLATE_ZOMBIE_KNIGHT;
        }
        return "";
    }

    public static void createZombieKnight(Location location) {
        Message.className = "ZombieKnightSpawnEgg";
        Zombie zombie = location.getWorld().spawn(location, Zombie.class);
        zombie.setCustomName(ChatColor.GOLD + Message.getTranslate(TEXT_ID_ZOMBIE_KNIGHT, DEFAULT_TRANSLATE_ZOMBIE_KNIGHT));

        zombie.getEquipment().setItemInMainHand(new ItemStack(Material.GOLDEN_SWORD));
        zombie.getEquipment().setHelmet(new ItemStack(Material.GOLDEN_HELMET));
        zombie.getEquipment().setChestplate(new ItemStack(Material.GOLDEN_CHESTPLATE));
        zombie.getEquipment().setLeggings(new ItemStack(Material.GOLDEN_LEGGINGS));
        zombie.getEquipment().setBoots(new ItemStack(Material.GOLDEN_LEGGINGS));
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(event.getHand() != null && event.getHand().equals(EquipmentSlot.HAND)){
                if(event.getItem() != null && event.getItem().getItemMeta() != null && event.getItem().getItemMeta().getLore() != null && event.getItem().getItemMeta().getLore().contains(ItemManager.ZombieKnightSpawnEgg.getItemMeta().getLore().get(0))){
                    Location spawnLocation;
                    if(event.getClickedBlock().isPassable()){
                        spawnLocation = event.getClickedBlock().getLocation().add(0.5,0,0.5);
                    }
                    else {
                        spawnLocation = event.getClickedBlock().getRelative(event.getBlockFace()).getLocation().add(0.5,0,0.5);
                    }
                    createZombieKnight(spawnLocation);
                    if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE)){
                        event.getItem().setAmount(event.getItem().getAmount() - 1);
                    }
                    event.setCancelled(true);
                }
            }
        }
    }
}
