package org.gravicapa.gravicapa.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class DatabaseManager {
    private String jdbcUrl;
    private String username;
    private String password;

    public DatabaseManager() {
        loadConfig();
    }

    public DatabaseManager(String jdbcUrl, String username, String password) {
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
    }

    private void loadConfig() {
        Properties properties = new Properties();
        try (InputStream is = getClass().getClassLoader().getResourceAsStream("config.properties")) {
            properties.load(is);
            jdbcUrl = properties.getProperty("jdbc.url");
            username = properties.getProperty("jdbc.username");
            password = properties.getProperty("jdbc.password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUserLocale(String name) {
        String locale = null;
        String sql = "SELECT l.locale FROM language l JOIN user u ON l.id = u.language_id WHERE u.name = ?";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, name);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    locale = resultSet.getString("locale");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return locale;
    }

    public int getLanguageIdByLocale(String locale) {
        int languageId = 0;
        String sql = "SELECT id FROM language WHERE locale = ?";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, locale);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    languageId = resultSet.getInt("id");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return languageId;
    }

    public void createUser(String name, int languageId) {
        String sql = "INSERT INTO user (name, language_id) VALUES (?, ?)";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, name);
            statement.setInt(2, languageId);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUserLanguageId(String name, int languageId) {
        String sql = "UPDATE user SET language_id = ? WHERE name = ?";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, languageId);
            statement.setString(2, name);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addLanguage(String locale) {
        String sql = "INSERT INTO language (locale) VALUES (?)";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, locale);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
