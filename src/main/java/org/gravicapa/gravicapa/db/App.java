package org.gravicapa.gravicapa.db;

public class App {

    static DatabaseManager dbManager = new DatabaseManager();
    static String username = "test123";

    static String newLanguage = "en";

    static int languageId;

    public static void main(String[] args) {

        // 1. enter to game
        if(isUserExist()){
            String userLocale = dbManager.getUserLocale(username);
            System.out.println(userLocale);
        }

        // 2. change user language
        if(isLanguageExist()){
            dbManager.updateUserLanguageId(username, languageId);
            System.out.println("User language updated successfully");
        }

    }

    private static boolean isLanguageExist() {
        languageId = dbManager.getLanguageIdByLocale(newLanguage);
        if (languageId == 0 ){
            dbManager.addLanguage(newLanguage);
            System.out.println("Language added successfully");

            isLanguageExist();
        }
        return true;
    }


    public static boolean isUserExist(){
        String userLocale = dbManager.getUserLocale(username);
        System.out.println("userLocale " + userLocale );

        if(userLocale == null){
            int languageId = dbManager.getLanguageIdByLocale("en");
            System.out.println("Language ID: " + languageId);

            dbManager.createUser(username, languageId);
            System.out.println("User created successfully");

            isUserExist();
        }
        return true;
    }

}
