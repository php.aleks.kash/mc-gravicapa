package org.gravicapa.gravicapa.db;

import org.gravicapa.gravicapa.core.message.Language;

public class UserRepository {

    private static final DatabaseManager dbManager = new DatabaseManager();
    public static String username;
    public static String userLocale;
    static String language = "en"; // for changeLanguage()
    static int languageId;


    public static void changeLanguage() {
        if(isLanguageExist()){
            dbManager.updateUserLanguageId(username, languageId);
            System.out.println("User language updated successfully");
        }
    }

    private static boolean isLanguageExist() {
        languageId = dbManager.getLanguageIdByLocale(language);
        if (languageId == 0 ){
            System.out.println("Language doesn't exist... will be added.  ");
            dbManager.addLanguage(language);
            System.out.println("Language added successfully");
            isLanguageExist();
        }
        return true;
    }

    public static boolean isUserExist(){
        String userLocale = dbManager.getUserLocale(username);
        if(userLocale == null){
            System.out.println("User doesn't exist... will be added.  ");
            if(isLanguageExist()){
                dbManager.createUser(username, languageId);
                System.out.println("User created successfully");
                isUserExist();
            }
        }
        return true;
    }

    public static String getUserLocale(){
        if(isUserExist()){
            return dbManager.getUserLocale(username);
        }
        return Language.DEFAULT.getLocale();
    }

}
