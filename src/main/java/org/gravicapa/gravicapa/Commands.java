package org.gravicapa.gravicapa;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.gravicapa.gravicapa.entity.ItemManager;
import org.gravicapa.gravicapa.entity.LeapingSpider;

public class Commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd
            , String label, String[] args) {
        if(sender instanceof Player player){

            if(player.isOp()){
                if(cmd.getName().equalsIgnoreCase("leapingspider")){
                    LeapingSpider.createLeapingSpider(player.getLocation());
                }

                if(cmd.getName().equalsIgnoreCase("givezombieknightspawnegg")){
                    ItemManager.createZombieKnightSpawnEgg();
                    player.getInventory().addItem(ItemManager.ZombieKnightSpawnEgg);
                }

//                if(cmd.getName().equalsIgnoreCase("titleconfig")){
//                    TitlePlugin.sendTitle(player, sender, args);
//                }
//
//                if(cmd.getName().equalsIgnoreCase("gravicapacreatemessage")){
//                    Message.create(sender);
//                }
            }
            else {
                player.sendMessage("You are not allowed to use this command!");
            }
            return true;
        }
        else {
            sender.sendMessage("Only players can use that command.");
            return true;
        }
    }

}
