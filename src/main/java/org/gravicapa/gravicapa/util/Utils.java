package org.gravicapa.gravicapa.util;

public class Utils {

    public static void printResultToConsole(String methodName, boolean result, String... entities) {
        StringBuilder message = new StringBuilder(methodName + ": ");

        if (entities.length > 0) {
            for (int i = 0; i < entities.length; i++) {
                message.append("\"").append(entities[i]).append("\"");

                if (i < entities.length - 1) {
                    message.append(" -> ");
                }
            }
        }

        if (result) {
            message.append(" found!");
        } else {
            message.append(" doesn't exist!");
        }

        System.out.println(message);
    }
}
