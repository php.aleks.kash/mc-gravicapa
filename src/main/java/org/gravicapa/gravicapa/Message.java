package org.gravicapa.gravicapa;

import org.gravicapa.gravicapa.core.yaml.YamlChecker;
import org.gravicapa.gravicapa.core.yaml.YamlController;
import org.gravicapa.gravicapa.core.yaml.YamlInit;
import org.gravicapa.gravicapa.core.yaml.YamlReader;
import org.gravicapa.gravicapa.db.UserRepository;

import java.io.File;

import static org.gravicapa.gravicapa.core.message.LanguageDetection.getTextLanguage;

public class Message {

    public static String className;

    public static Gravicapa plugin;

    public static String filePath;

    public static String getTranslate(String textId, String translation) {
        String result = "";

        initVariables(textId, translation);

        if(YamlChecker.checkYaml()){
            setLocaleForRead();
            result = YamlReader.read();
        } else {
            YamlController.chooseAction();
            getTranslate(textId, translation);
        }

       return result;
    }

    public static void setLocaleForRead(){
        YamlInit.localeToCheck = UserRepository.userLocale;
    }

    private static void initVariables(String textId,
                                      String translation){
        YamlChecker.verificationStage = 0;

        File dir = new File(plugin.getDataFolder().toURI());
        System.out.println("className -> " + className);
        System.out.println("textId -> " + textId);
        System.out.println("getTextLanguage(translation) -> " + getTextLanguage(translation));
        System.out.println("translation -> " + translation);
        System.out.println("filePath -> " + filePath);
        System.out.println("dir -> " + dir);

        YamlInit.init(className, textId,
                getTextLanguage(translation),
                translation, filePath,
                dir);
    }

}
