package org.gravicapa.gravicapa;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.gravicapa.gravicapa.core.message.Constant;
import org.gravicapa.gravicapa.db.UserRepository;
import org.gravicapa.gravicapa.entity.LeapingSpider;
import org.gravicapa.gravicapa.entity.ZombieKnightSpawnEgg;

public final class Gravicapa extends JavaPlugin {

    String filePath;

    {
        this.filePath = this.getDataFolder() + "\\" + Constant.FILENAME.getValue();
    }

    public String getFilePath() {
        return filePath;
    }

    @Override
    public void onEnable() {
        System.out.println("gravicapa plugin start!");
        System.out.println("++++");

        Message.plugin = this;
        Message.filePath = this.getFilePath();

        createSpider();
        createZombieKnightSpawnEgg();
        
        runPlayerJoinEvent();

        System.out.println("++++");
    }

    private class PlayerJoinListener implements Listener {

        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            Player player = event.getPlayer();
            String playerName = player.getName();
            String playerLanguage = player.getLocale();

            System.out.println("playerName " + playerName);
            System.out.println("playerLanguage " + playerLanguage);

            getLocaleFromDB(playerName);

            System.out.println("UserRepository.userLocale " + UserRepository.userLocale);
        }
    }

    public void getLocaleFromDB(String playerName){
        UserRepository.username = playerName;
        UserRepository.userLocale = UserRepository.getUserLocale();
    }

    public void changeUserLanguage(){
        UserRepository.changeLanguage();
    }

    public void runPlayerJoinEvent(){
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
    }

    public void createZombieKnightSpawnEgg(){
        getCommand("givezombieknightspawnegg").setExecutor(new Commands());
        getServer().getPluginManager().registerEvents(new ZombieKnightSpawnEgg(),this);
    }

    public void createSpider(){
        LeapingSpider leapingSpider = new LeapingSpider(this);
        getServer().getPluginManager().registerEvents(leapingSpider, this);
        getCommand("leapingspider").setExecutor(new Commands());
    }

    @Override
    public void onDisable() {
        System.out.println("My plugin stop!");
    }
}
