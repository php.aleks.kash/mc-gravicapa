package org.gravicapa.gravicapa.core.message;

public class MessageTranslation {
    private String fileName;
    private String textID;
    private String translation;

    public MessageTranslation() {}

    public MessageTranslation( String textID, String translation, String fileName) {
        this.fileName = fileName;
        this.textID = textID;
        this.translation = translation;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTextID() {
        return textID;
    }

    public void setTextID(String textID) {
        this.textID = textID;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String toString() {
        return "Message{" +
                "fileName='" + fileName + '\'' +
                ", textID='" + textID + '\'' +
                ", Translation='" + translation + '\'' +
                '}';
    }
}
