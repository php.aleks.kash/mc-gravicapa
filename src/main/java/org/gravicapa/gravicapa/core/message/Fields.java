package org.gravicapa.gravicapa.core.message;

public enum Fields {
    FILENAME("filename"),
    TEXTS("texts"),
    TEXT_ID("textId"),
    ;

    private final String value;

    Fields(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
