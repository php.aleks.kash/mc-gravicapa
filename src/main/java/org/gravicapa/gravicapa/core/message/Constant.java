package org.gravicapa.gravicapa.core.message;

public enum Constant {

    FILENAME("translate.yaml"),
    FOLDER_EXISTS("Folder already exists."),
    FOLDER_CREATED("Folder has been created."),
    FILE_EXISTS("File already exists."),
    FILE_CREATED("File has been created."),
    SUCCESS_DATA_WRITE("Data was successfully written."),
    FAILED_DATA_WRITE("Data was not recorded."),

    SUCCESS_FILE_WRITE_TO_YAML("New file successfully added to YAML file."),
    FAILED_FILE_ADD_TO_YAML("Failed to add new file to yaml file."),

    SUCCESS_TEXT_ADD_TO_YAML("New text successfully added to YAML file."),
    FAILED_TEXT_ADD_TO_YAML("Failed to add new text to YAML file."),

    SUCCESS_ADD_TRANSLATE_TO_YAML("New translation successfully added to YAML file."),
    FAILED_ADD_TRANSLATE_TO_YAML("Failed to add new translation to YAML file."),
    ;


    private String value;

    Constant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
