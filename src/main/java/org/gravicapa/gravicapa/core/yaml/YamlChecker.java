package org.gravicapa.gravicapa.core.yaml;

import org.gravicapa.gravicapa.core.message.Fields;
import org.gravicapa.gravicapa.util.Utils;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

public class YamlChecker {
    public static Map<String, Object> mapFoundFilename;
    public static Map<String, Object> mapFoundText;
    public static String textValue;
    public static int verificationStage;

    public static void main(String[] args) {
//        String filenameToCheck = "Leaping Spider";
//        String textIdToCheck = "leaping_green_spider";
//        String localeToCheck = "en";
//        String translationToCheck = "Leaping Green Spider";
        String filenameToCheck = "ZombieKnightSpawnEgg";
        String textIdToCheck = "zombie_knight";
        String localeToCheck = "en";
        String translationToCheck = "Zombie Knight";

        String yamlFilePath = "H:\\Programing\\projects\\minecraft\\Minecraft_Server\\plugins\\gravicapa\\translate.yaml";

        File dir = new File("H:\\Programing\\projects\\minecraft\\Minecraft_Server\\plugins\\gravicapa");

        YamlInit.init(filenameToCheck, textIdToCheck
                , localeToCheck, translationToCheck, yamlFilePath, dir);

        YamlController.isSuccessfulCheck = YamlChecker.checkYaml();
        System.out.println(YamlChecker.verificationStage);

        YamlController.chooseAction();
    }

    public static boolean checkYaml(){
        return isYamlExists() &&
                !isFileEmpty() &&
                YamlChecker.isFilenameExists() &&
                YamlChecker.isTextIdExists() &&
                YamlChecker.isLocaleTranslationExists() &&
                YamlChecker.isTranslationEqual();
    }

    private static boolean isFileEmpty() {
        verificationStage++;
        return YamlInit.fileToCheck.length() == 0;
    }

    private static boolean isYamlExists() {
        verificationStage++;
        return YamlInit.fileToCheck.exists();
    }

    public static boolean isTranslationEqual(){
        verificationStage++;
        if (textValue.equals(YamlInit.translationToCheck)) {
            Utils.printResultToConsole("isTranslationEqual", true, YamlInit.filenameToCheck, YamlInit.textIdToCheck, YamlInit.localeToCheck, textValue);
            return true;
        }
        Utils.printResultToConsole("isTranslationEqual", false, YamlInit.filenameToCheck, YamlInit.textIdToCheck, YamlInit.localeToCheck, YamlInit.translationToCheck);
        return false;
    }

    public static boolean isLocaleTranslationExists() {
        verificationStage++;
        textValue = (String) mapFoundText.get(YamlInit.localeToCheck);
        if (textValue != null) {
            Utils.printResultToConsole("isLocaleTranslationExists", true, YamlInit.filenameToCheck, YamlInit.textIdToCheck, YamlInit.localeToCheck, textValue);
            return true;
        }
        Utils.printResultToConsole("isLocaleTranslationExists", false, YamlInit.textIdToCheck, YamlInit.localeToCheck);
        return false;
    }

    public static boolean isTextIdExists() {
        verificationStage++;
        List<Map<String, Object>> texts = (List<Map<String, Object>>) mapFoundFilename.get(Fields.TEXTS.getValue());
        for (Map<String, Object> text : texts) {
            String textId = (String) text.get(Fields.TEXT_ID.getValue());
            if (textId.equals(YamlInit.textIdToCheck)) {
                Utils.printResultToConsole("isTextIdExists", true, YamlInit.filenameToCheck, YamlInit.textIdToCheck);
                mapFoundText = text;
                return true;
            }
        }
        Utils.printResultToConsole("isTextIdExists", false, YamlInit.filenameToCheck, YamlInit.textIdToCheck);
        return false;
    }

    public static boolean isFilenameExists(){
        verificationStage++;
        try {
            FileInputStream inputStream = new FileInputStream(YamlInit.yamlFilePath);

            Yaml yaml = new Yaml();
            List<Map<String, Object>> fileList = yaml.load(inputStream);

            for (Map<String, Object> file : fileList) {
                String filename = (String) file.get(Fields.FILENAME.getValue());
                if (filename.equals(YamlInit.filenameToCheck)) {
                    Utils.printResultToConsole("isFilenameExists", true, YamlInit.filenameToCheck);
                    mapFoundFilename = file;
                    return true;
                }
            }
            Utils.printResultToConsole("isFilenameExists", false, YamlInit.filenameToCheck);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

}
