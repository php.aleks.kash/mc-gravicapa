package org.gravicapa.gravicapa.core.yaml;

import org.gravicapa.gravicapa.core.message.Constant;

import java.io.File;
import java.io.IOException;

public class YamlCreator {

    public static void create(){
        if(YamlInit.dir.exists()){
            System.out.println(Constant.FOLDER_EXISTS.getValue() + " -> " + YamlInit.dir.getAbsolutePath());
        } else {
            boolean created = YamlInit.dir.mkdir();
            if(created){
                System.out.println(Constant.FOLDER_CREATED.getValue());
            }
        }

        File file = new File(YamlInit.dir, Constant.FILENAME.getValue());
        if(file.exists()){
            System.out.println(Constant.FILE_EXISTS.getValue());
        } else {
            try {
                boolean isCreateFile = file.createNewFile();
                if(isCreateFile){
                    System.out.println(Constant.FILE_CREATED.getValue() + " -> " + file.getName());
                }
            } catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}
