package org.gravicapa.gravicapa.core.yaml;

import java.io.File;

public class YamlInit {

    public static String filenameToCheck;
    public static String textIdToCheck;
    public static String localeToCheck;
    public static String translationToCheck;
    public static String yamlFilePath;
    public static File fileToCheck;
    public static File dir;

    public static void init(String filenameToCheck,
                            String textIdToCheck,
                            String localeToCheck,
                            String translationToCheck,
                            String yamlFilePath,
                            File dir){
        YamlInit.filenameToCheck = filenameToCheck;
        YamlInit.textIdToCheck = textIdToCheck;
        YamlInit.localeToCheck = localeToCheck;
        YamlInit.translationToCheck = translationToCheck;
        YamlInit.yamlFilePath = yamlFilePath;
        YamlInit.fileToCheck = new File(yamlFilePath);
        YamlInit.dir = dir;
    }

    public static void init(String filenameToCheck, String textIdToCheck,
                            String localeToCheck, String yamlFilePath){
        init(filenameToCheck, textIdToCheck, localeToCheck,
                null, yamlFilePath, null);
    }

}
