package org.gravicapa.gravicapa.core.yaml;

public class YamlController {
    public static boolean isSuccessfulCheck;

    public static void chooseAction(){
        switch (YamlChecker.verificationStage) {
            case 1:
                YamlCreator.create();
                YamlUpdater.addFilename();
                break;
            case 2:
            case 3:
                YamlUpdater.addFilename();
                break;
            case 4:
                YamlUpdater.addTextId();
                break;
            case 5:
                YamlUpdater.addLocaleTranslation();
                break;
            case 6:
                if (!isSuccessfulCheck) {
                    YamlUpdater.updateLocaleTranslation();
                }
                break;
            default:
                break;
        }
    }

}
