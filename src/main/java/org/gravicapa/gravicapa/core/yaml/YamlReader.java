package org.gravicapa.gravicapa.core.yaml;

import org.gravicapa.gravicapa.core.message.Language;
import org.yaml.snakeyaml.Yaml;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class YamlReader {

    public static void main(String[] args) {
        String yamlFilePath = "H:\\Programing\\projects\\minecraft\\Minecraft_Server\\plugins\\gravicapa\\translate.yaml";
        String filenameToCheck = "LeapingSpider";
        String textIdToCheck = "leaping_spider";
        String localeToCheck = "en";

        YamlInit.init(filenameToCheck, textIdToCheck, localeToCheck, yamlFilePath);
        System.out.println(YamlReader.read());
    }

    public static String read(){
        Yaml yaml = new Yaml();
        List<Map<String, Object>> fileList;
        try {
            fileList = yaml.load(new FileReader(YamlInit.yamlFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

        String translation = findTranslation(fileList, YamlInit.filenameToCheck, YamlInit.textIdToCheck);
        if (translation != null) {
            System.out.println("Translation: " + translation);
        } else {
            System.out.println("Translation not found.");
        }
        return translation;
    }

    private static String findTranslation(List<Map<String, Object>> fileList, String filename, String textId) {
        String translatedText = null;
        for (Map<String, Object> file : fileList) {
            String currentFilename = (String) file.get("filename");
            if (currentFilename.equals(filename)) {
                List<Map<String, Object>> texts = (List<Map<String, Object>>) file.get("texts");
                for (Map<String, Object> text : texts) {
                    String currentTextId = (String) text.get("textId");
                    if (currentTextId.equals(textId)) {
                        translatedText = (String) text.get(YamlInit.localeToCheck);
                        if (translatedText == null) {
                            if (YamlInit.localeToCheck.equals(Language.EN.getLocale())) {
                                translatedText = (String) text.get(Language.RU.getLocale());
                            } else if (YamlInit.localeToCheck.equals(Language.RU.getLocale())){
                                translatedText = (String) text.get(Language.EN.getLocale());
                            } else {
                                translatedText = (String) text.get(Language.DEFAULT.getLocale());
                            }
                        }

                    }
                }
            }
        }
        return translatedText;
    }

}
