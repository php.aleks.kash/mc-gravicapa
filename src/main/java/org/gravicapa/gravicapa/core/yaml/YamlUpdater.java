package org.gravicapa.gravicapa.core.yaml;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YamlUpdater {

    public static void addFilename(){
        String yamlFilePath = YamlInit.yamlFilePath;

        Map<String, Object> newText = new HashMap<>();
        newText.put("textId", YamlInit.textIdToCheck);
        newText.put(YamlInit.localeToCheck, YamlInit.translationToCheck);

        Yaml yaml = new Yaml();
        List<Map<String, Object>> fileList;
        try {
            fileList = yaml.load(new FileReader(yamlFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (fileList == null) {
            fileList = new ArrayList<>();
        }

        Map<String, Object> newFile = new HashMap<>();
        newFile.put("filename", YamlInit.filenameToCheck);
        List<Map<String, Object>> texts = new ArrayList<>();
        texts.add(newText);
        newFile.put("texts", texts);

        fileList.add(newFile);

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);

        Representer representer = new Representer();
        representer.addClassTag(newText.getClass(), Tag.MAP);

        yaml = new Yaml(representer, options);
        try {
            FileWriter writer = new FileWriter(yamlFilePath);
            yaml.dump(fileList, writer);
            writer.close();
            System.out.println("Value added to YAML file successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addTextId() {
        String yamlFilePath = YamlInit.yamlFilePath;
        String newTextId = YamlInit.textIdToCheck;

        Yaml yaml = new Yaml();
        List<Map<String, Object>> fileList;
        try {
            fileList = yaml.load(new FileReader(yamlFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        for (Map<String, Object> file : fileList) {
            String filename = (String) file.get("filename");
            if (filename.equals(YamlInit.filenameToCheck)) {
                List<Map<String, Object>> texts = (List<Map<String, Object>>) file.get("texts");
                Map<String, Object> newText = new HashMap<>();
                newText.put("textId", newTextId);
                newText.put(YamlInit.localeToCheck, YamlInit.translationToCheck);

                texts.add(newText);
                break;
            }
        }

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);

        Representer representer = new Representer();
        representer.addClassTag(newTextId.getClass(), Tag.STR);

        yaml = new Yaml(representer, options);
        try {
            FileWriter writer = new FileWriter(yamlFilePath);
            yaml.dump(fileList, writer);
            writer.close();
            System.out.println("Value added to YAML file successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addLocaleTranslation(){
            String yamlFilePath = YamlInit.yamlFilePath;
            String newLanguage = YamlInit.localeToCheck;

            Yaml yaml = new Yaml();
            List<Map<String, Object>> fileList;
            try {
                fileList = yaml.load(new FileReader(yamlFilePath));
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            for (Map<String, Object> file : fileList) {
                String filename = (String) file.get("filename");
                if (filename.equals(YamlInit.filenameToCheck)) {
                    List<Map<String, Object>> texts = (List<Map<String, Object>>) file.get("texts");
                    for (Map<String, Object> text : texts) {
                        String textId = (String) text.get("textId");
                        if (textId.equals(YamlInit.textIdToCheck)) {
                            text.put(newLanguage, YamlInit.translationToCheck);
                            break;
                        }
                    }
                    break;
                }
            }

            DumperOptions options = new DumperOptions();
            options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
            options.setPrettyFlow(true);

            Representer representer = new Representer();
            representer.addClassTag(newLanguage.getClass(), Tag.STR);

            yaml = new Yaml(representer, options);
            try {
                FileWriter writer = new FileWriter(yamlFilePath);
                yaml.dump(fileList, writer);
                writer.close();
                System.out.println("Value added to YAML file successfully.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    public static void updateLocaleTranslation(){
        String yamlFilePath = YamlInit.yamlFilePath;
        String updatedText = YamlInit.translationToCheck;

        Yaml yaml = new Yaml();
        List<Map<String, Object>> fileList;
        try {
            fileList = yaml.load(new FileReader(yamlFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        for (Map<String, Object> file : fileList) {
            String filename = (String) file.get("filename");
            if (filename.equals(YamlInit.filenameToCheck)) {
                List<Map<String, Object>> texts = (List<Map<String, Object>>) file.get("texts");
                for (Map<String, Object> text : texts) {
                    String textId = (String) text.get("textId");
                    if (textId.equals(YamlInit.textIdToCheck)) {
                        text.put(YamlInit.localeToCheck, updatedText);
                        break;
                    }
                }
                break;
            }
        }

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);

        Representer representer = new Representer();
        representer.addClassTag(updatedText.getClass(), Tag.STR);

        yaml = new Yaml(representer, options);
        try {
            FileWriter writer = new FileWriter(yamlFilePath);
            yaml.dump(fileList, writer);
            writer.close();
            System.out.println("The translation has been successfully updated in the YAML file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
